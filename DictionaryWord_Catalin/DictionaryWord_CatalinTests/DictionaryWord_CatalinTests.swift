//
//  DictionaryWord_CatalinTests.swift
//  DictionaryWord_CatalinTests
//
//  Created by Intern Device1 on 03/09/2018.
//  Copyright © 2018 Intern Device1. All rights reserved.
//

import XCTest
@testable import DictionaryWord_Catalin

class DictionaryWord_CatalinTests: XCTestCase {
    
    /*override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }*/
    
    func testPasswordValidation() {
        XCTAssertTrue("abcd".validatePassword())
        XCTAssertFalse("xp".validatePassword())
    }
    
}
