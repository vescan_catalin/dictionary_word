//
//  WordCheckBoxDetailCell.swift
//  DictionaryWord_Catalin
//
//  Created by Intern Device1 on 13/09/2018.
//  Copyright © 2018 Intern Device1. All rights reserved.
//

import UIKit

class WordCheckBoxDetailCell: UICollectionViewCell, SetWordDetailCell {

    @IBOutlet weak var wordCheckBoxImage: UIImageView!
    @IBOutlet weak var forgetOrLearnButton: UIButton!
    
    weak var delegate: CellDelegate?
    var id: String?
    var buttonFlag: Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func set(_ isKnown: Bool?, _ text: String?) {
        
        if isKnown == true {
            buttonFlag = true
            forgetOrLearnButton.setTitle("forget word", for: .normal)
            wordCheckBoxImage.image = UIImage(named: "checkedBox")
        } else {
            buttonFlag = false
            forgetOrLearnButton.setTitle("learn word", for: .normal)
            wordCheckBoxImage.image = UIImage(named: "uncheckedBox")
        }
        
        self.layer.borderWidth = 0.5
    }

    @IBAction func forgetOrLearn(_ sender: UIButton) {
        if forgetOrLearnButton.isSelected {
            forgetOrLearnButton.isEnabled = false
        }
        
        buttonFlag ? delegate?.forgetWord(wordId: id) : delegate?.learnWord(wordId: id)
        
    }
}
