//
//  FlagsEnum.swift
//  DictionaryWord_Catalin
//
//  Created by Intern Device1 on 07/09/2018.
//  Copyright © 2018 Intern Device1. All rights reserved.
//

import Foundation

enum UserDefaultsEnum {
    static let isLoggedIn = "isLoggedIn"
    static let userName = "userName"
    static let userEmail = "userEmail"
    static let userPhoto = "userPhoto"
    
    static let authorizationRequired = "authorizationRequired"
}
