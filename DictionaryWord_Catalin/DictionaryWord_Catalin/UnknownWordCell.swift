//
//  XibTableViewCell.swift
//  DictionaryWord_Catalin
//
//  Created by Intern Device1 on 07/09/2018.
//  Copyright © 2018 Intern Device1. All rights reserved.
//

import UIKit

class UnknownWordCell: UITableViewCell {
    
    @IBOutlet weak var wordImageView: UIImageView!
    
    @IBOutlet weak var wordNameLabel: UILabel!
    @IBOutlet weak var wordDefinitionLabel: UILabel!
    @IBOutlet weak var wordTypeLabel: UILabel!
    
    @IBOutlet weak var view: UIView!
    
    weak var delegate: CellDelegate?
    var id: String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCell(word: Word) {
        wordNameLabel.text = word.value
        wordTypeLabel.text = word.type
        wordDefinitionLabel.text = word.description
        id = word.id
        
        guard let urlImage = word.images?[0] else {
            wordImageView.image = UIImage(named: "apple")
            return
        }
        
        wordImageView.downloaded(from: urlImage)
    }
    
    @IBAction func learnWord(_ sender: UIButton) {
        delegate?.learnWord(wordId: id)
    }
}
