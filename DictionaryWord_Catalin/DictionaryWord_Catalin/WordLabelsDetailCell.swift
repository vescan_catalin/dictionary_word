//
//  WordLabelsDetailCell.swift
//  DictionaryWord_Catalin
//
//  Created by Intern Device1 on 13/09/2018.
//  Copyright © 2018 Intern Device1. All rights reserved.
//

import UIKit

class WordLabelsDetailCell: UICollectionViewCell, SetWordDetailCell {
    
    @IBOutlet weak var wordLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func set(_: Bool?, _ text: String?) {
        guard let text = text else {
            wordLabel.isHidden = true
            return
        }
        
        wordLabel.isHidden = false
        wordLabel.text = text
        wordLabel.layer.borderWidth = 0.5
    }
    
}
