//
//  WordStructure.swift
//  DictionaryWord_Catalin
//
//  Created by Intern Device1 on 06/09/2018.
//  Copyright © 2018 Intern Device1. All rights reserved.
//

import Foundation

struct Word: Codable {
    var id: String?
    var value: String?
    var description: String?
    var images: [String]?
    var joke: String?
    var type: String?
    var isKnown: Bool?
}

struct KnownWord: Codable {
    var knownWord: String?
}
