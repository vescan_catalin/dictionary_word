//
//  WordsTableCell.swift
//  DictionaryWord_Catalin
//
//  Created by Intern Device1 on 07/09/2018.
//  Copyright © 2018 Intern Device1. All rights reserved.
//

import UIKit

class LearnedWordCell: UITableViewCell {

    @IBOutlet weak var wordImage: UIImageView!
    
    @IBOutlet weak var wordLabel: UILabel!
    @IBOutlet weak var wordJokeLabel: UILabel!
    
    weak var delegate: CellDelegate?
    var id: String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setCell(word: Word) {
        wordLabel.text = word.value
        wordJokeLabel.text = word.joke
        id = word.id
     
        guard let urlImage = word.images?[0] else {
            wordImage.image = UIImage(named: "apple")
            return
        }
        
        wordImage.downloaded(from: urlImage)
    }
    
    @IBAction func forgetWord(_ sender: UIButton) {
        delegate?.forgetWord(wordId: id)
    }
}
