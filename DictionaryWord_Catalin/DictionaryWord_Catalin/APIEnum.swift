//
//  APIEnum.swift
//  DictionaryWord_Catalin
//
//  Created by Intern Device1 on 06/09/2018.
//  Copyright © 2018 Intern Device1. All rights reserved.
//

import Foundation

enum APIEnum {
    static let loginLink = "https://us-central1-memoword-44150.cloudfunctions.net/memoWordApi/signin"
    static let linkToWords = "https://us-central1-memoword-44150.cloudfunctions.net/memoWordAuthorizedApi/words"
    static let linkToLearnWord = "https://us-central1-memoword-44150.cloudfunctions.net/memoWordAuthorizedApi/know"
    static let linkToForgetWord = "https://us-central1-memoword-44150.cloudfunctions.net/memoWordAuthorizedApi/forget"
}
