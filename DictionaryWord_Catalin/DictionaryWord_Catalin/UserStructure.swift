//
//  UserStructure.swift
//  DictionaryWord_Catalin
//
//  Created by Intern Device1 on 05/09/2018.
//  Copyright © 2018 Intern Device1. All rights reserved.
//

import Foundation

struct User: Codable {
    var email: String
    var password: String
}

struct APILoginResponse: Codable {
    var uid: String
    var name: String
    var photoURL: String
    var email: String
    var token: String
}

