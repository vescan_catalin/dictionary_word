//
//  WordImageDetailCell.swift
//  DictionaryWord_Catalin
//
//  Created by Intern Device1 on 13/09/2018.
//  Copyright © 2018 Intern Device1. All rights reserved.
//

import UIKit

class WordImageDetailCell: UICollectionViewCell, SetWordDetailCell {
    
    @IBOutlet weak var wordImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func set(_: Bool?, _ urlImage: String?) {
        guard let urlImage = urlImage else {
            wordImage.image = UIImage(named: "apple")
            return
        }
        
        wordImage.downloaded(from: urlImage)
        wordImage.layer.borderWidth = 0.5
        
    }
    
}
