//
//  ProfileController.swift
//  DictionaryWord_Catalin
//
//  Created by Intern Device1 on 18/09/2018.
//  Copyright © 2018 Intern Device1. All rights reserved.
//

import UIKit
import Photos

class ProfileController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet weak var profileLabel: UILabel!
    @IBOutlet weak var closeProfileButton: UIButton!
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var shadowImage: UIImageView!
    let tapAction = UITapGestureRecognizer()
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailHeaderLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    var imagePicker = UIImagePickerController()
    
    var userName: String?
    var userEmail: String?
    var userPhoto: String?
    var imagePath: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userName = UserDefaults.standard.object(forKey: UserDefaultsEnum.userName) as? String
        userEmail = UserDefaults.standard.object(forKey: UserDefaultsEnum.userEmail) as? String
        userPhoto = UserDefaults.standard.object(forKey: UserDefaultsEnum.userPhoto) as? String
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        checkImageExistence()
        setLabels()
        setImage()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        tapAction.addTarget(self, action: #selector(ProfileController.addImage))
        profileImage.addGestureRecognizer(tapAction)
        profileImage.isUserInteractionEnabled = true
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        coordinator.animate(alongsideTransition: { _ in
            self.setImage()
        }, completion: nil)
    }
    
    @objc func addImage() {
        if UserDefaults.standard.bool(forKey: UserDefaultsEnum.authorizationRequired){
            if let url = URL(string: UIApplicationOpenSettingsURLString) {
                if UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
            }
        }
        
        let actionSheetController = UIAlertController(title: "Images", message: "Select image source...", preferredStyle: .actionSheet)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) {(action) in}
        actionSheetController.addAction(cancelAction)
        
        let takePictureAction = UIAlertAction(title: "Camera", style: .default) {(action) in}
        actionSheetController.addAction(takePictureAction)
        
        let choosePictureAction = UIAlertAction(title: "Photos", style: .default) {
            (action) in
            PHPhotoLibrary.requestAuthorization({(status:PHAuthorizationStatus) in
                switch status{
                case .authorized:
                    DispatchQueue.main.async {
                        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                            self.imagePicker.delegate = self
                            self.imagePicker.sourceType = .photoLibrary
                            self.imagePicker.allowsEditing = false
                            
                            self.present(self.imagePicker, animated: true, completion: nil)
                        }
                        
                        UserDefaults.standard.set(false, forKey: UserDefaultsEnum.authorizationRequired)
                    }
                    break
                case .denied:
                    DispatchQueue.main.async {
                        UserDefaults.standard.set(true, forKey: UserDefaultsEnum.authorizationRequired)
                    }
                    break
                default:
                    DispatchQueue.main.async {
                        
                    }
                    break
                }
            })
        }
        actionSheetController.addAction(choosePictureAction)
        
        present(actionSheetController, animated: true, completion: nil)
        
    }
    
    func checkImageExistence() {
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = NSURL(fileURLWithPath: path)
        if let pathComponent = url.appendingPathComponent("profilePicture.png") {
            if FileManager.default.fileExists(atPath: pathComponent.path) {
                profileImage.image = UIImage(contentsOfFile: pathComponent.path)
            } else {
                PopupAlert.popup.createAlert(controller: self, title: "Error", message: "Image not found!")
                profileImage.image = UIImage(named: "apple")
            }
        } else {
            PopupAlert.popup.createAlert(controller: self, title: "Error", message: "File path not available!")
            profileImage.image = UIImage(named: "apple")
        }
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            profileImage.image = pickedImage
            
            saveImage(imageName: "profilePicture.png", image: pickedImage)
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func saveImage(imageName: String, image: UIImage) {
        // get the documents directory url
        if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            // create the destination file url to save your image
            let fileURL = documentsDirectory.appendingPathComponent(imageName)
            // get your UIImage jpeg data representation and check if the destination file url already exists
            if let data = UIImagePNGRepresentation(image) {
                do {
                    // writes the image data to disk
                    try data.write(to: fileURL)
                } catch {
                    PopupAlert.popup.createAlert(controller: self, title: "Error", message: "Couldn't save image!")
                }
            }
        }
    }
    
    func setImage() {
        view.layoutIfNeeded()
        //profileImage.image = UIImage(named: "apple")
        profileImage.layer.cornerRadius = profileImage.bounds.height / 2
        
        profileImage.clipsToBounds = true
        profileImage.layer.borderWidth = 2
        profileImage.layer.borderColor = UIColor.black.cgColor
        profileImage.layer.masksToBounds = true
        
        shadowImage.backgroundColor = UIColor.clear
        shadowImage.layer.shadowColor = UIColor.gray.cgColor
        shadowImage.layer.shadowOffset = CGSize(width: 10.0, height: 10.0)
        shadowImage.layer.shadowOpacity = 0.5
        shadowImage.layer.shadowRadius = 2.0
        shadowImage.layer.shadowPath = UIBezierPath(roundedRect: shadowImage.bounds, cornerRadius: 100.0).cgPath
    }
    
    func setLabels() {
        guard let userName = self.userName, let userEmail = self.userEmail else {
            PopupAlert.popup.createAlert(controller: self, title: "Error", message: "User not found!")
            return
        }
        
        nameLabel.attributedText = setAtributes(string: userName, fontType: UIFont.boldSystemFont(ofSize: 24),
                                                fontColor: UIColor.blue, underline: nil)
        
        emailHeaderLabel.attributedText = setAtributes(string: "Email:", fontType: UIFont.systemFont(ofSize: 16), fontColor: UIColor.gray,
                                                       underline: NSUnderlineStyle.styleSingle.rawValue)
        
        let splittedEmail = userEmail.split(separator: "@")
        if !splittedEmail.isEmpty && splittedEmail.count == 2 {
            let firstPartEmailAttribute = setAtributes(string: String(splittedEmail[0]), fontType: UIFont.systemFont(ofSize: 18),
                                                       fontColor: UIColor.green, underline: nil)
            let linkElementAttribute = setAtributes(string: "@", fontType: UIFont.systemFont(ofSize: 18),
                                                    fontColor: UIColor.black, underline: nil)
            let secondPartEmailAttribute = setAtributes(string: String(splittedEmail[1]), fontType: UIFont.systemFont(ofSize: 18),
                                                        fontColor: UIColor.red, underline: nil)
            linkElementAttribute.append(secondPartEmailAttribute)
            firstPartEmailAttribute.append(linkElementAttribute)
            
            
            emailLabel.attributedText = firstPartEmailAttribute
        }
    }
    
    func setAtributes(string: String, fontType: UIFont, fontColor: UIColor, underline: Int?) -> NSMutableAttributedString {
        guard let _ = underline else {
            let attribute = [NSAttributedStringKey.font: fontType, NSAttributedStringKey.foregroundColor: fontColor]
            
            return NSMutableAttributedString(string: string, attributes: attribute)
        }
        
        let attribute = [NSAttributedStringKey.font: fontType, NSAttributedStringKey.foregroundColor: fontColor,
                         kCTUnderlineStyleAttributeName: underline as Any] as [AnyHashable : Any]
        
        return NSMutableAttributedString(string: string, attributes: attribute as? [NSAttributedStringKey : Any])
    }
    
    @IBAction func closeProfile(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
