//
//  ViewController.swift
//  DictionaryWord_Catalin
//
//  Created by Intern Device1 on 03/09/2018.
//  Copyright © 2018 Intern Device1. All rights reserved.
//

import UIKit
import KeychainSwift

class ViewController: UIViewController, UITextFieldDelegate {
    
    // MARK: Properties
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var shadowImage: UIImageView!
    
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passLabel: UILabel!
    @IBOutlet weak var passTextField: UITextField!
    
    @IBOutlet weak var activityIndicatorView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var logInButton: UIButton!
    
    @IBOutlet weak var constraintContentHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var centerConstraintHeight: NSLayoutConstraint!
    
    var activeField: UITextField!
    var lastOffset: CGPoint!
    var keyboardHeight: CGFloat!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        setupLabels()
        setupButtons()
        
        emailTextField.delegate = self
        passTextField.delegate = self
        
        // Observe keyboard change
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    func setupViews() {
        logoImageView.layer.borderWidth = 3
        logoImageView.layer.borderColor = UIColor.red.cgColor
        logoImageView.layer.cornerRadius = logoImageView.frame.size.height/2
        logoImageView.clipsToBounds = true
        
        shadowImage.backgroundColor = UIColor.clear
        shadowImage.layer.shadowColor = UIColor.gray.cgColor
        shadowImage.layer.shadowOffset = CGSize(width: 10.0, height: 10.0)
        shadowImage.layer.shadowOpacity = 0.5
        shadowImage.layer.shadowRadius = 2.0
        shadowImage.layer.shadowPath = UIBezierPath(roundedRect: shadowImage.bounds, cornerRadius: 100.0).cgPath
    }
    
    func setupLabels() {
        guard let stringEmail = emailLabel.text, let stringPassword = passLabel.text else {return}
        emailLabel.attributedText = NSAttributedString(string: stringEmail, attributes: [.underlineStyle:
            NSUnderlineStyle.styleSingle.rawValue, .underlineColor: UIColor.gray])
        passLabel.attributedText = NSAttributedString(string: stringPassword, attributes: [.underlineStyle:
            NSUnderlineStyle.styleSingle.rawValue, .underlineColor: UIColor.gray])
    }
    
    func setupButtons() {
        logInButton.layer.borderWidth = 3
        logInButton.layer.borderColor = UIColor.blue.cgColor
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeField = textField
        
        lastOffset = self.scrollView.contentOffset
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        activeField.resignFirstResponder()
        
        activeField = nil
        
        return true
    }
    
    // keyboard observers
    @objc func keyboardWillShow(notification: NSNotification) {
        if keyboardHeight != nil {
            return
        }
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            keyboardHeight = keyboardSize.height
            
            // increase content view height by keyboard height
            guard let keyboardHeight = self.keyboardHeight else {
                PopupAlert.popup.createAlert(controller: self, title: "Error", message: "Keyboard not found!")
                return
            }
            
            if UIDevice.current.orientation.isPortrait {
                UIView.animate(withDuration: 0.3, animations: {
                    self.constraintContentHeight.constant += keyboardHeight
                    self.constraintButtonHeight.constant += 2 * keyboardHeight
                    self.centerConstraintHeight.constant -= keyboardHeight
                })
                
                let distanceToBottom = self.scrollView.frame.size.height -
                    activeField.frame.origin.y - activeField.frame.size.height
                
                let colapseSpace = keyboardHeight - distanceToBottom
                
                if colapseSpace < 0 {
                    return
                }
                
                // new offset for scrollView
                UIView.animate(withDuration: 0.3, animations: {
                    self.scrollView.contentOffset = CGPoint(x: self.lastOffset.x, y: colapseSpace - 50)
                })
            } else {
                UIView.animate(withDuration: 0.3, animations: {
                    self.constraintContentHeight.constant += keyboardHeight
                    self.constraintButtonHeight.constant += keyboardHeight
                    self.centerConstraintHeight.constant -= (1/2) * keyboardHeight
                })
                
                let distanceToBottom = self.scrollView.frame.size.height -
                    activeField.frame.origin.y - activeField.frame.size.height
                
                let colapseSpace = keyboardHeight - distanceToBottom
                
                if colapseSpace < 0 {
                    return
                }
                
                // new offset for scrollView
                UIView.animate(withDuration: 0.3, animations: {
                    self.scrollView.contentOffset = CGPoint(x: self.lastOffset.x, y: colapseSpace - 10)
                })
            }
            
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        guard let keyboardHeight = self.keyboardHeight else {
            PopupAlert.popup.createAlert(controller: self, title: "Error", message: "Keyboard not found!")
            return
        }
        
        if UIDevice.current.orientation.isPortrait {
            UIView.animate(withDuration: 0.3, animations: {
                self.constraintContentHeight.constant -= keyboardHeight
                self.constraintButtonHeight.constant -= 2 * keyboardHeight
                self.centerConstraintHeight.constant += keyboardHeight
                
                self.scrollView.contentOffset = self.lastOffset
            })
        } else {
            UIView.animate(withDuration: 0.3, animations: {
                self.constraintContentHeight.constant -= keyboardHeight
                self.constraintButtonHeight.constant -= keyboardHeight
                self.centerConstraintHeight.constant += (1/2) * keyboardHeight
                
                self.scrollView.contentOffset = self.lastOffset
            })
        }
        
        self.keyboardHeight = nil
    }
    
    //Mark: Actions
    @IBAction func logIn(_ sender: UIButton) {
        manageActivityIndicator(flag: true)
        
        //        guard let email = emailTextField.text, !email.isEmpty,
        //            let password = passTextField.text, !password.isEmpty,
        //            email.validateEmail() && password.validatePassword()
        //            else {
        //                PopupAlert.popup.createAlert(controller: self, title: "Error", message: "Wrong Email or Password!")
        //                emailTextField.text = ""
        //                passTextField.text = ""
        //                manageActivityIndicator(flag: false)
        //                return
        //        }
        let email = "sandu20@sandu.com"
        let password = "12345678"
        
        APIService().getConnection(email: email, password: password, completionHandler: { response, error in
            
            DispatchQueue.main.async{
                self.manageActivityIndicator(flag: false)
                guard let userInformations = response else {
                    PopupAlert.popup.createAlert(controller: self, title: "ERROR", message: "Authorisation failed!")
                    return
                }
                
                // save token to UserDefaults
                guard let token = response?.token  else {
                    PopupAlert.popup.createAlert(controller: self, title: "Error", message: "Invalid token!")
                    return
                }
                KeychainSwift().set(token, forKey: KeychainEnum.loginToken)
                
                UserDefaults.standard.set(true, forKey: UserDefaultsEnum.isLoggedIn)
                UserDefaults.standard.set(userInformations.name, forKey: UserDefaultsEnum.userName)
                UserDefaults.standard.set(userInformations.email, forKey: UserDefaultsEnum.userEmail)
                UserDefaults.standard.set(userInformations.photoURL, forKey: UserDefaultsEnum.userPhoto)
                
                self.performSegue(withIdentifier: "presentNextScreen", sender: self)
            }
        })
    }
    
    // if the flag is true activity indicator will appear
    func manageActivityIndicator(flag: Bool) {
        if flag {
            view.bringSubview(toFront: activityIndicatorView)
            activityIndicator.startAnimating()
        } else {
            view.sendSubview(toBack: self.activityIndicatorView)
            activityIndicator.stopAnimating()
        }
    }
    
}
