//
//  APIService.swift
//  DictionaryWord_Catalin
//
//  Created by Intern Device1 on 05/09/2018.
//  Copyright © 2018 Intern Device1. All rights reserved.
//

import Foundation
import KeychainSwift

class APIService {
    
    func getConnection(email: String, password: String, completionHandler: @escaping (APILoginResponse?, Error?) -> Void) {
        
        guard let url = URL(string: APIEnum.loginLink) else {
            completionHandler(nil, NSError(domain:"", code:401, userInfo:
                [NSLocalizedDescriptionKey: "Wrong URL!"]))
            return
        }
        
        var urlRequest = URLRequest(url: url)
        
        urlRequest.httpMethod = "POST"
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        // set up the session
        let session = URLSession.shared
        
        do {
            let body = try JSONEncoder().encode(User(email: email, password: password))
            urlRequest.httpBody = body
        } catch {
            completionHandler(nil, error)
            return
        }
        
        // make the request
        let task = session.dataTask(with: urlRequest) {
            (data, response, error) in
            // check data
            guard let responseData = data else {
                completionHandler(nil, NSError(domain:"", code:401, userInfo:
                    [NSLocalizedDescriptionKey: "Can't receive any data!"]))
                return
            }
            do {
                let userResponse = try JSONDecoder().decode(APILoginResponse.self, from: responseData)
                completionHandler(userResponse, nil)
            } catch {
                completionHandler(nil, error)
            }
        }
        task.resume()
    }
    
    func getRequest(completionHandler: @escaping ([Word]?, Error?) -> Void) {
        
        guard let url = URL(string: APIEnum.linkToWords) else {
            completionHandler(nil, NSError(domain:"", code:401, userInfo:
                [NSLocalizedDescriptionKey: "Wrong URL!"]))
            return
        }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "GET"
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        guard let token = KeychainSwift().get(KeychainEnum.loginToken) else {
            completionHandler(nil, NSError(domain:"", code:401, userInfo:
                [NSLocalizedDescriptionKey: "Invalid token!"]))
            return
        }
        urlRequest.addValue("Bearer " + token, forHTTPHeaderField: "Authorization")
        
        // set up the session
        let session = URLSession.shared
        
        // make the request
        let task = session.dataTask(with: urlRequest) { data, response, error in
            
            guard let receivedData = data else {
                completionHandler(nil, NSError(domain:"", code:401, userInfo:
                    [NSLocalizedDescriptionKey: "Can't receive any data!"]))
                return
            }
            do {
                // convert JSON format into a list of Words
                let words = try JSONDecoder().decode([Word].self, from: receivedData)
                completionHandler(words, nil)
            } catch {
                completionHandler(nil, error)
            }
        }
        task.resume()
    }
    
    func learnOrFogetWord(url: String, id: String?, completionHandler: @escaping ([Word]?, Error?) -> Void) {
        
        guard let urlReq = URL(string: url) else {
            completionHandler(nil, NSError(domain:"", code:401, userInfo:
                [NSLocalizedDescriptionKey: "Wrong URL!"]))
            return
        }
        
        var urlRequest = URLRequest(url: urlReq)
        urlRequest.httpMethod = "POST"
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        guard let token = KeychainSwift().get(KeychainEnum.loginToken) else {
            completionHandler(nil, NSError(domain:"", code:401, userInfo:
                [NSLocalizedDescriptionKey: "Invalid token!"]))
            return
        }
        urlRequest.addValue("Bearer " + token, forHTTPHeaderField: "Authorization")
        
        // set up the session
        let session = URLSession.shared
        
        do {
            let body = try JSONEncoder().encode(KnownWord(knownWord: id))
            urlRequest.httpBody = body
        } catch {
            completionHandler(nil, NSError(domain:"", code:401, userInfo:
                [NSLocalizedDescriptionKey: "Unserializable data!"]))
            return
        }
        
        // request
        let task = session.dataTask(with: urlRequest) { data, response, error in

            guard let receivedData = data else {
                completionHandler(nil, error)
                return
            }
            do {
                // convert JSON format into a list of Words
                let words = try JSONDecoder().decode([Word].self, from: receivedData)
                completionHandler(words, nil)
            } catch {
                completionHandler(nil, error)
            }
        }
        task.resume()
    }
    
}
