//
//  WordListViewController.swift
//  DictionaryWord_Catalin
//
//  Created by Intern Device1 on 06/09/2018.
//  Copyright © 2018 Intern Device1. All rights reserved.
//

import UIKit

protocol CellDelegate: class {
    func learnWord(wordId: String?)
    func forgetWord(wordId: String?)
}

class WordListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, CellDelegate {
    @IBOutlet weak var wordTableView: UITableView!
    @IBOutlet weak var activityIndicatorView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var learnedWords = [Word]()
    var unknownWords = [Word]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        wordTableView.delegate = self
        wordTableView.dataSource = self
        
        loadNib(nibName: "UnknownWordCell", cellIdentifier: "unknownWordCellIdentifier")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setNavigationBar()
        getWords()
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? "Learned words" : "Unknown words"
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // first section is for learned words
        // second section is for unknown words
        return section == 0 ? learnedWords.count : unknownWords.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            guard let prototypeCell = tableView.dequeueReusableCell(withIdentifier: "prototypeWordCellIdentifier",
                                                                    for: indexPath) as? LearnedWordCell else {
                                                                        fatalError("The dequeued cell is not an instance of LearnedWordCell.")
            }
            
            let knownWord = learnedWords[indexPath.row]
            
            prototypeCell.delegate = self
            prototypeCell.setCell(word: knownWord)
            
            return prototypeCell
        } else {
            guard let xibCell = tableView.dequeueReusableCell(withIdentifier: "unknownWordCellIdentifier",
                                                              for: indexPath) as? UnknownWordCell  else {
                                                                fatalError("The dequeued cell is not an instance of UnknownWordCell.")
            }
            
            let unknownWord = unknownWords[indexPath.row]
            
            xibCell.delegate = self
            xibCell.setCell(word: unknownWord)
            
            return xibCell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension // Custom row height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard.init(name: "WordDetail", bundle: nil)
        
        if let viewController = storyboard.instantiateViewController(withIdentifier:
            "wordDetailController") as? WordDetailController {
            switch indexPath.section {
            case 0: viewController.word = learnedWords[indexPath.row]
            case 1: viewController.word = unknownWords[indexPath.row]
            default: break
            }
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    @IBAction func viewProfile(_ sender: UIBarButtonItem) {
        let storyboard = UIStoryboard.init(name: "Profile", bundle: nil)
        
        if let profileController = storyboard.instantiateViewController(withIdentifier:
            "profileIdentifier") as? ProfileController {
            self.navigationController?.present(profileController, animated: true, completion: nil)
        }
    }
    
    func setNavigationBar() {
        let nav = self.navigationController?.navigationBar
        
        nav?.barStyle = UIBarStyle.black
        nav?.tintColor = UIColor.lightGray
        
        navigationItem.title = "Words"
        navigationItem.rightBarButtonItem?.title = "Profile"
    }
    
    func learnWord(wordId: String?) {
        manageActivityIndicator(flag: true)
        
        guard let wordId = wordId else {
            manageActivityIndicator(flag: false)
            PopupAlert.popup.createAlert(controller: self, title: "Error", message: "Wrong id!")
            return
        }
        
        APIService().learnOrFogetWord(url: APIEnum.linkToLearnWord, id: wordId, completionHandler: { response, error in
            DispatchQueue.main.async {
                self.manageActivityIndicator(flag: false)
                
                guard let listOfWords = response else {
                    PopupAlert.popup.createAlert(controller: self, title: "ERROR", message: "No data received!")
                    return
                }
                
                self.learnedWords.removeAll()
                self.unknownWords.removeAll()

                self.learnedWords = listOfWords.filter({ (word) -> Bool in
                    word.isKnown == true
                })
                self.unknownWords = listOfWords.filter({ (word) -> Bool in
                    word.isKnown == false
                })
                
                self.wordTableView.reloadData()
            }
        })
    }
    
    func forgetWord(wordId: String?) {
        manageActivityIndicator(flag: true)
        
        guard let wordId = wordId else {
            manageActivityIndicator(flag: false)
            PopupAlert.popup.createAlert(controller: self, title: "Error", message: "Wrong id!")
            return
        }
        
        APIService().learnOrFogetWord(url: APIEnum.linkToForgetWord, id: wordId, completionHandler: { response, error in
            DispatchQueue.main.async {
                self.manageActivityIndicator(flag: false)
                
                guard let listOfWords = response else {
                    PopupAlert.popup.createAlert(controller: self, title: "ERROR", message: "No data received!")
                    return
                }
                
                self.learnedWords.removeAll()
                self.unknownWords.removeAll()
                
                self.learnedWords = listOfWords.filter({ (word) -> Bool in
                    word.isKnown == true
                })
                self.unknownWords = listOfWords.filter({ (word) -> Bool in
                    word.isKnown == false
                })
                
                self.wordTableView.reloadData()
            }
        })
    }
    
    func getWords(){
        manageActivityIndicator(flag: true)
        
        APIService().getRequest(completionHandler: { words, error in
            DispatchQueue.main.async {
                self.manageActivityIndicator(flag: false)
                
                guard let listOfWords = words else {
                    PopupAlert.popup.createAlert(controller: self, title: "ERROR", message: "No more words!");
                    return
                }
                
                self.learnedWords = listOfWords.filter({ (word) -> Bool in
                    word.isKnown == true
                })
                self.unknownWords = listOfWords.filter({ (word) -> Bool in
                    word.isKnown == false
                })
                
                self.wordTableView.reloadData()
            }
        })
    }
    
    func loadNib(nibName: String, cellIdentifier: String) {
        let nib = UINib(nibName: nibName, bundle: nil)
        wordTableView.register(nib, forCellReuseIdentifier: cellIdentifier)
    }
    
    // if the flag is true activity indicator will appear
    func manageActivityIndicator(flag: Bool) {
        if flag {
            view.bringSubview(toFront: activityIndicatorView)
            activityIndicator.startAnimating()
        } else {
            view.sendSubview(toBack: self.activityIndicatorView)
            activityIndicator.stopAnimating()
        }
    }
    
}
