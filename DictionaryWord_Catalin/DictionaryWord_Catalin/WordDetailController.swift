//
//  WordDetailController.swift
//  DictionaryWord_Catalin
//
//  Created by Intern Device1 on 13/09/2018.
//  Copyright © 2018 Intern Device1. All rights reserved.
//

import Foundation
import UIKit

protocol SetWordDetailCell: class {
    func set(_: Bool?, _: String?)
}

class WordDetailController: UIViewController, UICollectionViewDataSource,
UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, CellDelegate {
    @IBOutlet weak var wordCollectionView: UICollectionView!
    @IBOutlet weak var activityIndicatorView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var word: Word?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        wordCollectionView.delegate = self
        wordCollectionView.dataSource = self
        
        loadNib(nibName: "WordImageDetailCell", cellIdentifier: "wordImageIdentifier")
        loadNib(nibName: "WordLabelsDetailCell", cellIdentifier: "wordLabelsIdentifier")
        loadNib(nibName: "WordCheckBoxDetailCell", cellIdentifier: "wordChecBoxIdentifier")
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return word?.joke == nil ? 5 : 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.row {
        case 0: do {
            guard let wordImage = collectionView.dequeueReusableCell(withReuseIdentifier: "wordImageIdentifier",
                                                                     for: indexPath) as? WordImageDetailCell else {
                                                                        fatalError("The dequeued cell is not an instance of WordImageDetailCell.")
            }
            if word?.images?.count == 0 {
                wordImage.set(nil, nil)
            } else if let url = word?.images?[0] {
                wordImage.set(nil, url)
            }
            
            return wordImage
            }
        case 1: do {
            guard let wordName = collectionView.dequeueReusableCell(withReuseIdentifier: "wordLabelsIdentifier",
                                                                    for: indexPath) as? WordLabelsDetailCell else {
                                                                        fatalError("The dequeued cell is not an instance of WordLabelsDetailCell.")
            }
            if let name = word?.value {
                wordName.set(nil, name)
            }
            
            return wordName
            }
        case 2: do {
            guard let wordType = collectionView.dequeueReusableCell(withReuseIdentifier: "wordLabelsIdentifier",
                                                                    for: indexPath) as? WordLabelsDetailCell else {
                                                                        fatalError("The dequeued cell is not an instance of WordLabelsDetailCell.")
            }
            if let type = word?.type {
                wordType.set(nil, type)
            }
            
            return wordType
            }
        case 3: do {
            guard let knownUnknown = collectionView.dequeueReusableCell(withReuseIdentifier: "wordChecBoxIdentifier",
                                                                         for: indexPath) as? WordCheckBoxDetailCell else {
                                                                            fatalError("The dequeued cell is not an instance of WordCheckBoxDetailCell.")
            }
            if word?.isKnown == true {
                knownUnknown.set(word?.isKnown, "Learned")
            } else {
                knownUnknown.set(word?.isKnown, "Unknown")
            }
            
            knownUnknown.id = word?.id
            knownUnknown.delegate = self
            return knownUnknown
            }
        case 4: do {
            guard let wordDescription = collectionView.dequeueReusableCell(withReuseIdentifier: "wordLabelsIdentifier",
                                                                           for: indexPath) as? WordLabelsDetailCell else {
                                                                            fatalError("The dequeued cell is not an instance of WordLabelsDetailCell.")
            }
            if let description = word?.description {
                wordDescription.set(nil, description)
            }
            
            return wordDescription
            }
        case 5: do {
            guard let wordJoke = collectionView.dequeueReusableCell(withReuseIdentifier: "wordLabelsIdentifier",
                                                                    for: indexPath) as? WordLabelsDetailCell else {
                                                                        fatalError("The dequeued cell is not an instance of WordLabelsDetailCell.")
            }
            if let joke = word?.joke {
                wordJoke.set(nil, joke)
            }
            
            return wordJoke
            }
        default:  assert(false, "default ...")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch indexPath.row {
        case 0: return CGSize(width: wordCollectionView.bounds.width, height: 300)
        case 1: return CGSize(width: wordCollectionView.bounds.width, height: 50)
        case 2: return CGSize(width: wordCollectionView.bounds.width/2.5, height: 50)
        case 3: return CGSize(width: wordCollectionView.bounds.width/2.5, height: 50)
        case 4: return CGSize(width: wordCollectionView.bounds.width, height: 100)
        case 5: return CGSize(width: wordCollectionView.bounds.width, height: 100)
        default: return CGSize(width: 0, height: 0)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return wordCollectionView.bounds.width/6
    }
    
    func learnWord(wordId: String?) {
        manageActivityIndicator(flag: true)
        
        guard let wordId = wordId else {
            manageActivityIndicator(flag: false)
            PopupAlert.popup.createAlert(controller: self, title: "Error", message: "Wrong id!")
            return
        }
        
        APIService().learnOrFogetWord(url: APIEnum.linkToLearnWord, id: wordId, completionHandler: { response, error in
            DispatchQueue.main.async {
                self.manageActivityIndicator(flag: false)
                
                self.word?.isKnown = true
                self.wordCollectionView.reloadData()
            }
        })
    }
    
    func forgetWord(wordId: String?) {
        manageActivityIndicator(flag: true)
        
        guard let wordId = wordId else {
            manageActivityIndicator(flag: false)
            PopupAlert.popup.createAlert(controller: self, title: "Error", message: "Wrong id!")
            return
        }
        
        APIService().learnOrFogetWord(url: APIEnum.linkToForgetWord, id: wordId, completionHandler: { response, error in
            DispatchQueue.main.async {
                self.manageActivityIndicator(flag: false)
                
                self.word?.isKnown = false
                self.wordCollectionView.reloadData()
            }
        })
    }
    
    func loadNib(nibName: String, cellIdentifier: String) {
        let nib = UINib(nibName: nibName, bundle: nil)
        wordCollectionView.register(nib, forCellWithReuseIdentifier: cellIdentifier)
    }
    
    // if the flag is true activity indicator will appear
    func manageActivityIndicator(flag: Bool) {
        if flag {
            view.bringSubview(toFront: activityIndicatorView)
            activityIndicator.startAnimating()
        } else {
            view.sendSubview(toBack: self.activityIndicatorView)
            activityIndicator.stopAnimating()
        }
    }
}
