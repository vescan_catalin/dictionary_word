//
//  Extensions.swift
//  DictionaryWord_Catalin
//
//  Created by Intern Device1 on 04/09/2018.
//  Copyright © 2018 Intern Device1. All rights reserved.
//

import Foundation
import UIKit

extension String {
    func validateEmail() -> Bool {
        // the Email adress must not be empty
        guard !self.isEmpty else {
            return false
        }
        // this is the regex to match an Email adress
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: self)
    }
    
    func validatePassword() -> Bool {
        // the password must not be empty
        guard !self.isEmpty else {
            return false
        }
        // ~= -> check if the number is between 4 and 20
        return 4...20 ~= self.count ? true : false
    }
    
}

extension UIImageView {
    func downloaded(from url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
            }.resume()
    }
    func downloaded(from link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}


